//gol.js

// TODO: Disable row col edit when started
// When stopped: Click to toggle alive/dead

loop = 0
loopDiv = null

board = []
tempboard = []
rows = 5
cols = 15

timer = 0

var ClassAlive = "square alive"
var ClassDead = "square dead"

dbg = 1;
function startGame() {
  window.clearInterval(timer)
  timer = setInterval("uppdateCells()", 1000)
}

function stopGame() {
  window.clearInterval(timer)
}

function createBoard() {
	rows = document.getElementById('rows').value
	cols = document.getElementById('cols').value

	var life1 = document.getElementById('life')

  // Remove previous grid
  while (life1.firstChild) {
    life1.removeChild(life1.firstChild)
  }

  board = new Array(rows);// Helper array, easy indexable with just visible dom elements.
  tempboard = new Array(rows)
  for (r = 0; r<rows; r++) {
    board[r] = new Array(cols)
    tempboard[r] = new Array(rows)
    for ( c = 0; c<cols; c++) {
      d = document.createElement("div")
      if (Math.floor((Math.random()*3)) === 0) {
        d.className = ClassAlive
      } else {
        d.className = ClassDead
      }
      d.innerHTML = "&nbsp;"
      d.onclick = function(){toggleAlive(this)}
      life1.appendChild(d)
    	board[r][c] = d
    } 
    d = document.createElement("div")
    d.style.clear = 'both'
    life1.appendChild(d)
  }
  loopDiv = document.createElement("div")
  loopDiv.innerHTML = loop
  life1.appendChild(loopDiv)
  loop=0
  loopDiv.innerHTML = "Loop: " + loop
}

function toggleAlive(obj) {
  if (isAlive(obj)) {
    obj.className = ClassDead
  } else {
    obj.className = ClassAlive
  }
}

function isAlive(obj) {
  if (obj.className.indexOf("alive")  >= 0 ) {
    return true
  }
  return false
}

function noOfNeighbors(r,c) {
  var rb = Math.max(r-1,0)
  var re = Math.min(r+1,rows-1)
  var cb = Math.max(c-1,0)
  var ce = Math.min(c+1,cols-1)

  var n = 0
  for (rx = rb; rx<=re; rx += 1) {
    for (cx = cb; cx<=ce; cx += 1) {
      if (!(rx == r && cx == c) && isAlive(board[rx][cx])) {
        n += 1;
        if (n >= 4) break
      }
    }
  }
  return n;
}

function uppdateCells() {
	var life1 = document.getElementById('life');
	// Calc new values fo all cells before updating
  for ( r = 0; r<rows; r++) {
    for ( c = 0; c<cols; c++) {
      tempboard[r][c] = noOfNeighbors(r,c)
    }
  }

  // Update cells with new values
  var noOfChangedCells = 0
  for ( r = 0; r<rows; r++) {
    for ( c = 0; c<cols; c++) {
      var alive = isAlive(board[r][c])
      if (!alive) {
        if (tempboard[r][c] == 3) {
          board[r][c].className = ClassAlive
          noOfChangedCells += 1
        }
      } else {
        if (tempboard[r][c] < 2 || 3 < tempboard[r][c]) {
          board[r][c].className = ClassDead
          noOfChangedCells += 1
        }
      }
    }
  }

  if (noOfChangedCells === 0) {
    stopGame()
  } else {
    loop += 1
  }
  loopDiv.innerHTML = "Loop: " + loop
}
